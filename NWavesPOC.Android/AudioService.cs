﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using NWaves.Audio;
using NWaves.FeatureExtractors;
using NWaves.FeatureExtractors.Options;
using NWaves.Signals;
using NWavesPOC;
using Xamarin.Forms;

[assembly: Dependency(typeof(NWaves.DemoXamarin.Droid.DependencyServices.AudioService))]
namespace NWaves.DemoXamarin.Droid.DependencyServices
{
    public class AudioService : IAudioService
    {
        private int _frameSize = 512;
        private int _hopSize = 128;

        public IList<float> Analyze()
        {
            DiscreteSignal _signal;
            using (var sr = new StreamReader(Android.App.Application.Context.Assets.Open("audiocheck.net_sin_666Hz_1s.wav")))
            {
                using (var ms = new MemoryStream())
                {
                    sr.BaseStream.CopyTo(ms);
                    ms.Position = 0;
                    var waveFile = new WaveFile(ms);
                    _signal = waveFile[Channels.Left];
                }

            }

            var frameDuration = (double)_frameSize / _signal.SamplingRate;
            var hopDuration = (double)_hopSize / _signal.SamplingRate;
            var pitchOptions = new PitchOptions
            {
                SamplingRate = _signal.SamplingRate,
                FrameDuration = frameDuration,
                HopDuration = hopDuration,
                HighFrequency = 900
            };

            var pitchExtractor = new PitchExtractor(pitchOptions);
            return pitchExtractor.ParallelComputeFrom(_signal)
                                           .Select(p => p[0])
                                           .ToList();
        }
    }
}
