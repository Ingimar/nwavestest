﻿using System.Collections.Generic;

namespace NWavesPOC
{
    public interface IAudioService
    {
        IList<float> Analyze();
    }
}