﻿using System;
using System.ComponentModel;
using Xamarin.Forms;

namespace NWavesPOC
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        IAudioService _audioService;

        public MainPage()
        {
            InitializeComponent();

            _audioService = DependencyService.Get<IAudioService>();
        }

        void OnClicked(object sender, EventArgs args)
        {
            var result = _audioService.Analyze();
            listView.ItemsSource = result;
        }
    }
}
